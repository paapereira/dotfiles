#===============================================================================
#
#  _ __   __ _  __ _ _ __   ___ _ __ ___(_)_ __ __ _
# | '_ \ / _` |/ _` | '_ \ / _ \ '__/ _ \ | '__/ _` |
# | |_) | (_| | (_| | |_) |  __/ | |  __/ | | | (_| |
# | .__/ \__,_|\__,_| .__/ \___|_|  \___|_|_|  \__,_|
# |_|               |_|
#
# http://paapereira.xyz/
#
#===============================================================================

export PATH="$PATH:$(du "$HOME/storage/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//'):$HOME/.emacs.d/bin"

# Default programs
export EDITOR="/usr/bin/nvim"
# export FILES="/usr/bin/vifm"
export FILES="/usr/bin/nnn"
export TERMINAL="/usr/bin/kitty"
# export TERMINAL="/usr/bin/alacritty"
export BROWSER="/usr/bin/librewolf"
# export BROWSER="/usr/bin/firefox"

export XDG_CONFIG_HOME="$HOME/.config"

#eof
