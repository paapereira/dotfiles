#!/usr/bin/python2

import sys

ispy3 = sys.version_info >= (3, 0)

print(sys.stdin.buffer.read(5) if ispy3 else sys.stdin.read(5))


