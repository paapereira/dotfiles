#===============================================================================
#
#  _ __   __ _  __ _ _ __   ___ _ __ ___(_)_ __ __ _
# | '_ \ / _` |/ _` | '_ \ / _ \ '__/ _ \ | '__/ _` |
# | |_) | (_| | (_| | |_) |  __/ | |  __/ | | | (_| |
# | .__/ \__,_|\__,_| .__/ \___|_|  \___|_|_|  \__,_|
# |_|               |_|
#
# https://paapereira.xyz/
#
#===============================================================================

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
export GOPATH=$HOME/gopath
export PATH=$GOPATH:$GOPATH/bin:$PATH
export GOPATH=/usr/share/go
export PATH=$GOPATH:$GOPATH/bin:$PATH
export GOPATH=$HOME/go
export PATH=$GOPATH:$GOPATH/bin:$PATH

export QT_QPA_PLATFORMTHEME=qt5

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

#eof
