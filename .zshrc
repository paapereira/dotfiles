#===============================================================================
#
#  _ __   __ _  __ _ _ __   ___ _ __ ___(_)_ __ __ _
# | '_ \ / _` |/ _` | '_ \ / _ \ '__/ _ \ | '__/ _` |
# | |_) | (_| | (_| | |_) |  __/ | |  __/ | | | (_| |
# | .__/ \__,_|\__,_| .__/ \___|_|  \___|_|_|  \__,_|
# |_|               |_|
#
# https://paapereira.xyz/
#
#===============================================================================

#neofetch

# local tmp dir
mkdir -p ~/.local/tmp

# source "${HOME}/.zgen/robbyrussell/oh-my-zsh-master/plugins/z/z.sh"
# source "$HOME/.zgen/zsh-syntax-highlighting/zsh-syntax-highlighting.sh"

# alias
#alias gtdshell="bash ~/bin/gtdshell/gtdshell"
#alias g="bash ~/bin/gtdshell/gtdshell"
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
#alias journal='~/bin/journal/journal.sh'
alias j='journal log'
alias v=$EDITOR
alias vim=$EDITOR
alias o=xdg-open
alias parumenu='paru -Syu --upgrademenu'

# starship shell
eval "$(starship init zsh)"

# Vi mode
bindkey -v

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

#source /home/paapereira/.config/broot/launcher/bash/br

# export PAGER="/bin/sh -c \"unset PAGER;col -b -x | \
    # $EDITOR -R -c 'set ft=man nomod nolist' -c 'map q :q<CR>' \
    # -c 'map <SPACE> <C-D>' -c 'map b <C-U>' \
    # -c 'nmap K :Man <C-R>=expand(\\\"<cword>\\\")<CR><CR>' -\""

# export PAGER="/bin/sh -c \"unset PAGER;col -b -x | \
#     $EDITOR -R -c 'set ft=man nomod nolist' -\""

# source "$HOME/.config/nvim/plugged/gruvbox/gruvbox_256palette.sh"

# qqgtd
# source "$HOME/.local/bin/qqgtd/qqgtd"
#source ~/.qqgtdrc

# showkey -a
source "$HOME/storage/bin/dir"
bindkey -s '^F' 'dir\n'
bindkey -s '^O' 'open\n'

# nnn
export NNN_COLORS='#04e2f4ff'
alias nnn='nnn -de'

export HISTFILE=~/.zsh_history
export HISTSIZE=10000
export SAVEHIST=10000

FZ_HISTORY_CD_CMD=zshz

#====== zplug ==================================================================

if [[ ! -d ~/.zplug ]]; then
  git clone https://github.com/zplug/zplug ~/.zplug
  source ~/.zplug/init.zsh && zplug update --self
fi

source ~/.zplug/init.zsh

zplug "zplug/zplug", hook-build:'zplug --self-manage'

zplug "changyuheng/fz", defer:1
zplug "rupa/z", use:z.sh

zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-completions"
zplug "zsh-users/zsh-syntax-highlighting"
source "$HOME/storage/bin/zsh-syntax-highlighting/zsh-syntax-highlighting.sh"
zplug "zsh-users/zsh-history-substring-search"

zplug "jgogstad/passwordless-history"

zplug "mafredri/zsh-async", from:"github", use:"async.zsh"

zplug "chitoku-k/fzf-zsh-completions"

zplug "dracula/zsh", as:theme

if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# zplug load --verbose
zplug load

#eof
