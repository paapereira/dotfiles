typeset -U PATH path
path=("$HOME/storage/bin" "$path[@]")
export PATH
export PATH="$PATH:$(du "$HOME/storage/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
