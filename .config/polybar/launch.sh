#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# dpi for surface
if [ "`hostname`" = "tardis" ]; then
  custom_dpi=96
  custom_bar_height=30
  custom_title_len=150
  custom_tray_size=16
  custom_network=eno1
elif [ "`hostname`" = "surface-tardis" ]; then
  custom_dpi=168
  custom_bar_height=45
  custom_title_len=50
  custom_tray_size=40
  custom_network=wlp1s0
fi

for m in $(polybar --list-monitors | cut -d":" -f1); do
  MONITOR=$m HEIGHT=$custom_bar_height DPI=$custom_dpi TITLELEN=$custom_title_len TRAYSIZE=$custom_tray_size INTERFACE=$custom_network polybar --reload topbar &
  if [ -f ~/.config/polybar/showbottom ]; then
    MONITOR=$m HEIGHT=$custom_bar_height DPI=$custom_dpi TITLELEN=$custom_title_len TRAYSIZE=$custom_tray_size INTERFACE=$custom_network polybar --reload bottombar &
  fi
	#WIRELESS=$(ls /sys/class/net/ | grep ^wl | awk 'NR==1{print $1}') MONITOR=$m polybar --reload topbar &
done

#eof
